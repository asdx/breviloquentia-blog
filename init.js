let env = require("./env.config")
let fs = require("fs")

let CONFIG_JSON_PATH = `${__dirname}/src/config.json`
let UTF8_ENCODING = "utf8"

let content = JSON.stringify(env)

fs.writeFile(CONFIG_JSON_PATH, content, UTF8_ENCODING, (err) => {
  if (err) { throw err }
})
